<?php namespace Cars\WS2MS;


use GuzzleHttp\Client as HttpClient;
use GuzzleHttp\Exception\RequestException;

class WS2MS
{
    const ACCEPTANCE_BASE_URL = 'http://cars-ws2ms.test.9yd.nl/';
    const PRODUCTION_BASE_URL = 'https://cars-ws2ms.9yards.nl/';

    /**
     * Default timeout for the HTTP calls
     */
    const HTTP_TIMEOUT = 60;
    
    /**
     * HTTP client to make calls to the API
     *
     * @var HttpClient
     */
    protected $client;

    /**
     * The username to login to WS2MS
     *
     * @var string
     */
    protected $username;

    /**
     * The password used to login to WS2MS
     *
     * @var string
     */
    protected $password;

    /**
     * Instance of the token class containing the access token
     *
     * @var Token
     */
    protected $accessToken;

    /**
     * Instance of the token class containing the refresh token
     *
     * @var Token
     */
    protected $refreshToken;

    /**
     * The settings returned by the API
     *
     * @var Settings
     */
    protected $settings;

    /**
     * Creates a WS2MS client
     *
     * @param string $username The username used to login
     * @param string $password The password used to login
     * @param string $environment Determines the base URL to use, from live or test environment
     *     
     * @param array $clientOptions Custom options for the HTTP client
     */
    public function __construct($username, $password, $environment = 'production', array $clientOptions = [])
    {   
        //Save the login details
        $this->username = $username;
        $this->password = $password;
        
        $baseUri = self::PRODUCTION_BASE_URL;

        if($environment != 'production') {
            $baseUri = self::ACCEPTANCE_BASE_URL;
        }

        $ws2msOptions = [
            'base_uri' => $baseUri,
            'headers' => ['Content-Type' => 'application/json'],
            'timeout' => self::HTTP_TIMEOUT,
        ];

        // Create the HTTP client
        $this->client = new HttpClient(array_merge($clientOptions, $ws2msOptions));
    }

    /**
     * Accesses a service provided by WS2MS using the stored access token.
     *
     * @param string $apiCall The call that should be done to the API
     * @param array $data Data to post or the querystring to use in the url
     * @param string $method GET or POST request, default is GET
     */
    public function request($apiCall, $data = [], $method = 'GET')
    {
        $apiCall = str_replace('stockbase', 'carsws2ms', $apiCall);

        $this->init();

        $url = $this->settings->getUrl($apiCall);

        switch ($method) {
            case 'GET':
                $response = $this->client->get($url, array(
                    'headers' => array(
                        'Authentication' => $this->accessToken->getValue()
                    ),
                    'query' => $data
                ));

                break;
            case 'POST':
                $response = $this->client->post($url, array(
                    'headers' => array(
                        'Authentication' => $this->accessToken->getValue()
                    ),
                    'json' => $data
                ));

                break;
        }

        $json = json_decode($response->getBody());
        $body = $json->carsws2ms;



        // Check if all good
        if (!isset($body->response->content)) {
            $message = $body->response->answer . ": ";
            $message .= isset($body->response->message) ? $body->response->message : '';

            // If there was an error, throw an exception
            throw new \Exception($message);
        }

        // Return the content, without all noise that we only need in this library
        return $body->response->content;
    }

    /**
     * Download an image and put it in the path defined by destination
     *
     * @param string $url The URL to the image to download
     * @param string $destination Destination where the file should be saved
     */
    public function download($url, $destination)
    {
        $this->init();

        $this->client->get($url, array(
            'headers' => array(
                'Content-Type' => null,
                'Authentication' => $this->accessToken->getValue()
            ),
            'sink' => $destination
        ));
    }

    /**
     * Connect to the API and set up some keys
     */
    protected function init()
    {
        // Check if we already have a valid token
        if (!$this->accessToken || $this->accessToken->expired()) {
            $refreshSucceeded = true;

            if ($this->refreshToken && $this->refreshToken->getValue()) {
                try {
                    //We have a refresh token, try to refresh
                    if(!$this->refresh()) {
                        $refreshSucceeded = false;
                    }
                } catch (RequestException $e) {
                    $refreshSucceeded = false;
                }
            } else {
                // No token yet, so we need to login first
                $refreshSucceeded = false;
            }

            // Refresh failed, try to login first, then try again
            if (!$refreshSucceeded) {
                $this->login()->refresh();
            }
        }

        // Ask for the settings if we dont have them yet
        if (!$this->settings) {
            $response = $this->client->get('settings', array(
                'headers' => array(
                    'Authentication' => $this->accessToken->getValue()
                )
            ));

            $json = json_decode($response->getBody());
            $body = $json->carsws2ms;
            $services = [];
            foreach ($body->services as $service) {
                $services[] = $service->code;
            }

            $this->settings = new Settings($services, $body->settings_updated);
        }
    }

    /**
     * Try login with username and password provided
     */
    protected function login()
    {
        $response = $this->client->get('services/login', array(
            'headers' => array(
                'username'=> $this->username, 
                'password' => $this->password
            )
        ));

        $json = json_decode($response->getBody());
        $body = $json->carsws2ms;

        // Check if all good
        if (!isset($body->expiration_date)) {
            $message = $body->answer . ": ";
            $message .= isset($body->message) ? $body->message : '';

            // If there was an error, throw an exception
            throw new \Exception($message);
        }

        $expire = new \DateTime($body->expiration_date, new \DateTimezone('UTC'));

        $this->accessToken = new Token($body->access_token, $expire);
        $this->refreshToken = new Token($body->refresh_token);

        return $this;
    }

    /**
     * Refresh tokens
     */
    protected function refresh()
    {
        $token = $this->refreshToken;

        $response = $this->client->get('authenticate', array(
            'headers' => array(
                'Authentication' => $token->getValue()
            )
        ));

        $json = json_decode($response->getBody());
        $body = $json->carsws2ms;
        $expire = new \DateTime($body->expiration_date, new \DateTimezone('UTC'));

        $this->accessToken = new Token($body->access_token, $expire);
        if (isset($body->refresh_token)) {
            $this->refreshToken = new Token($body->refresh_token);
        }

        return $this;
    }
}
