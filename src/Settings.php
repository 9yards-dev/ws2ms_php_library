<?php namespace Cars\WS2MS;

class Settings
{
    /**
     * An array of the urls this account has access to.
     *
     * @var string[]
     */
    protected $urls;

    /**
     * The updated date of this revision of the settings.
     *
     * @var int
     */
    protected $updated;

    /**
     * Contains data returned by settings call of the API
     *
     * @param string[] $urls An array of urls available
     * @param int $updated When the call to settings is done
     */
    public function __construct(array $urls, $updated)
    {
        $this->urls = $urls;
        $this->updated = $updated;
    }

    /**
     * Gets the url of a service
     *
     * @param string $serviceName
     *
     * @return string
     */
    public function getUrl($serviceName)
    {
        if (in_array($serviceName, $this->urls)) {
            return str_replace('_', '/', $serviceName);
        } else {
            throw new \Exception("Service " . $serviceName . " is not available, these services are: " . json_encode($this->urls) . "");
        }
    }

    /**
     * Generates settings object from a json string
     *
     * @param string $data The data to generate the settings based on
     *
     * @return Settings The settings object generated
     */
    public static function fromJson($data)
    {
        $data = json_decode($data);

        return new static($data->urls, $data->updated);
    }
}
