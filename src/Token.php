<?php namespace Cars\WS2MS;

class Token
{
    /**
     * The token value.
     *
     * @var string
     */
    protected $value;

    /**
     * The expiration date of the token.
     *
     * @var \DateTime
     */
    protected $expire;

    /**
     * Creates a new instance of the Token class and defines the params
     *
     * @param string $value The token value
     * @param \DateTime $expire When (if) the token expires
     */
    public function __construct($value, \DateTime $expire = null)
    {
        $this->value = $value;
        $this->expire = $expire;
    }

    /**
     * Gets the token value
     *
     * @return string
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * Checks if the token is expired
     * If expire is not set, the token will not expire
     *
     * @param \DateTime $date This can be used to check the expiry with a custom date
     *
     * @return bool
     */
    public function expired(\DateTime $date = null)
    {
        // If expire is not set, it will never be expired
        if (!$this->expire) {
            return false;
        }

        //If date is not defined in parameters, use the current time
        $date = $date ?: new \DateTime('now', new \DateTimezone('UTC'));

        //It is expired if current time (or param time) is after the expiry time
        return ($date > $this->expire);
    }
}
